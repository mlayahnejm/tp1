package tn.rnu.dsi.tp1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

public class DSI31V2Activity extends AppCompatActivity {
    EditText aff;
    Button un,deux,trois,division;
    CheckBox b1,b2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.exemple2);
        aff=(EditText) findViewById(R.id.afficheur);
        un=(Button) findViewById(R.id.un);
        deux=(Button) findViewById(R.id.deux);
        trois=(Button) findViewById(R.id.trois);
        division=(Button) findViewById(R.id.division);
        b1=(CheckBox)findViewById(R.id.box1);
        b2=(CheckBox)findViewById(R.id.box2);
        un.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clic_chiffre(1);
            }
        });
        deux.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clic_chiffre(2);
            }
        });
        b1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if(b)
                    Toast.makeText(DSI31V2Activity.this,"vous êtes feminine",Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(DSI31V2Activity.this,"vous êtes masculin",Toast.LENGTH_SHORT).show();
            }
        });
        b2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b)
                    Toast.makeText(DSI31V2Activity.this,"vous êtes majeur",Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(DSI31V2Activity.this,"vous n'êtes pas majeur",Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void clic_chiffre(int b){
        if (aff.getText().toString().equals("0"))
            aff.setText(""+b);
        else
            aff.setText(aff.getText().toString() +""+ b);
    }
}