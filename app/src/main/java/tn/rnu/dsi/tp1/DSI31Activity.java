package tn.rnu.dsi.tp1;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class DSI31Activity extends AppCompatActivity implements View.OnClickListener {

    EditText aff;
    Button un,deux,trois,division;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.exemple2);
        aff=(EditText) findViewById(R.id.afficheur);
        un=(Button) findViewById(R.id.un);
        deux=(Button) findViewById(R.id.deux);
        trois=(Button) findViewById(R.id.trois);
        division=(Button) findViewById(R.id.division);
        un.setOnClickListener(this);
        deux.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view.getId()==R.id.un) {
            if (aff.getText().toString().equals("0"))
                aff.setText(un.getText().toString());
            else
                aff.setText(aff.getText().toString() + un.getText().toString());
        }
        if(view.getId()==R.id.deux) {
            if (aff.getText().toString().equals("0"))
                aff.setText(deux.getText().toString());
            else
                aff.setText(aff.getText().toString() + deux.getText().toString());
        }
    }
}
