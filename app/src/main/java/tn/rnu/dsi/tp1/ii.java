package tn.rnu.dsi.tp1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class ii extends AppCompatActivity {

    EditText taille,poids;
    RadioGroup group;
    RadioButton centimetre,metre;
    Button calcul,raz;
    public static boolean isNumeric(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch(NumberFormatException e){
            return false;
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dsi31);
        taille=(EditText) findViewById(R.id.taille);
        poids=(EditText) findViewById(R.id.poids);
        group=(RadioGroup) findViewById(R.id.group);
        centimetre=(RadioButton) findViewById(R.id.radio2);
        metre=(RadioButton) findViewById(R.id.radio1);
        calcul=(Button) findViewById(R.id.calcul);
        raz=(Button) findViewById(R.id.raz);

        raz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                poids.setText("");
                taille.setText("");
                Toast.makeText(ii.this,"Les champs de saisies sont réinitialisées",Toast.LENGTH_SHORT).show();
            }
        });
        calcul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isNumeric(poids.getText().toString()) && isNumeric(taille.getText().toString()))
                {
                    double p=Double.parseDouble(poids.getText().toString());
                    double t=Double.parseDouble(taille.getText().toString());
                    if(group.getCheckedRadioButtonId()==R.id.radio2)
                    {
                        t=t/100;
                    }
                    double imc=p/(t*t);
                    String interpretation;
                    if(imc<16.5)
                        interpretation="Dénutrition";
                    else if(imc<18.5)
                        interpretation="Maigreur";
                    else if(imc<25)
                        interpretation="Corpulence normale";
                    else if(imc<30)
                        interpretation="Surpoids";
                    else if(imc<35)
                        interpretation="Obésité moderée";
                    else
                        interpretation="Obésité morbide ou massive";
                    Toast.makeText(ii.this,interpretation,Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}