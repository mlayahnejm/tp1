package tn.rnu.dsi.tp1;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class iiv2 extends AppCompatActivity {

    EditText taille,poids;
    RadioGroup group;
    RadioButton centimetre,metre;
    Button calcul,raz;
    public static boolean isNumeric(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch(NumberFormatException e){
            return false;
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dsi31);
        taille=(EditText) findViewById(R.id.taille);
        poids=(EditText) findViewById(R.id.poids);
        group=(RadioGroup) findViewById(R.id.group);
        centimetre=(RadioButton) findViewById(R.id.radio2);
        metre=(RadioButton) findViewById(R.id.radio1);
        calcul=(Button) findViewById(R.id.calcul);
        raz=(Button) findViewById(R.id.raz);

        raz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                poids.setText("");
                taille.setText("");
                Toast.makeText(iiv2.this,"Les champs de saisies sont réinitialisées",Toast.LENGTH_SHORT).show();
            }
        });
        calcul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isNumeric(poids.getText().toString()) && isNumeric(taille.getText().toString()))
                {
                    double p=Double.parseDouble(poids.getText().toString());
                    double t=Double.parseDouble(taille.getText().toString());
                    Intent i=new Intent(iiv2.this,enfant.class);
                    i.putExtra("poids",p);
                    i.putExtra("taille",t);
                    startActivityForResult(i,1);

                }
            }
        });
    }
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(resultCode==RESULT_OK) {
            Toast.makeText(this, "Retour avec succés", Toast.LENGTH_LONG).show();
            String operateur=data.getStringExtra("operateur");
            double res;
            if(operateur.equals("somme")){
                res=Double.parseDouble(poids.getText().toString())+Double.parseDouble(taille.getText().toString());
            }

            else
                res=Double.parseDouble(poids.getText().toString())-Double.parseDouble(taille.getText().toString());
            String s="Le resultat est" + res;
            Toast.makeText((Context) this,  s,Toast.LENGTH_LONG).show();
        }
        else
            Toast.makeText(this,"Retour avec echec",Toast.LENGTH_LONG).show();

    }

}