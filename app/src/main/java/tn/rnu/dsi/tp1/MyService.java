package tn.rnu.dsi.tp1;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.widget.Toast;

public class MyService extends Service {
    public MyService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Bundle info = intent.getExtras();
        if (info != null) {
            float p = info.getFloat("poids");
            float t = info.getFloat("taille");
            float res = p / (t * t);
            String affiche = null;
            if (res < 16.5)
                affiche = "dénutrition";
            else if (res < 18.5)
                affiche = "Maigreur";
            else if (res < 25)
                affiche = "corpulence normale";
            else if (res < 30)
                affiche = "surpoids";
            else if (res < 35)
                affiche = "obésité modérée";
            else
                affiche = "obésité morbide ou massive";

            Toast.makeText(getBaseContext(), affiche, Toast.LENGTH_LONG).show();

        }
        return START_STICKY;
    }
}