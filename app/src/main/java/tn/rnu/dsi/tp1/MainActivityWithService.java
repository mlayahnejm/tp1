package tn.rnu.dsi.tp1;


import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.CheckBox;
import android.widget.Toast;
import android.content.Intent;
public class MainActivityWithService extends Activity {
    private final String defaut = "Vous devez cliquer sur le bouton  Calculer lIMC  pour obtenir un résultat.";
    Button calculer = null;
    Button raz = null;
    EditText poids = null;
    EditText taille = null;
    RadioGroup group = null;
    //TextView result = null;
    CheckBox mega = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dsi31);
        calculer = (Button)findViewById(R.id.calcul);
        calculer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tai = taille.getText().toString();
                String poi = poids.getText().toString();
                float t = Float.valueOf(tai);
                if(t == 0)
                    Toast.makeText(getApplicationContext(), "taille non coherente", Toast.LENGTH_SHORT).show();
                else {
                    float p = Float.valueOf(poi);
                    // On vérifie le radio bouton sélectionnée
                    if(group.getCheckedRadioButtonId() == R.id.radio2)
                        t = t / 100;

                    Bundle sac = new Bundle();
                    sac.putFloat("poids", p);
                    sac.putFloat("taille",t);

                    Intent intent = new Intent(MainActivityWithService.this, MyService.class);
                    intent.putExtras(sac);

                    startService(intent);

                }

            }
        });
        raz = (Button)findViewById(R.id.raz);
        raz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                taille.setText("");
                poids.setText("");
            }
        });
        taille = (EditText)findViewById(R.id.taille);
        poids = (EditText)findViewById(R.id.poids);
        mega = (CheckBox)findViewById(R.id.mega);
        group = (RadioGroup)findViewById(R.id.group);
        //result = (TextView)findViewById(R.id.result);
        //on utilise des valeurs pour verifier, si le clavier ne fonctionne pas
        poids.setText("70");
        taille.setText("160");
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        //noinspection SimplifiableIfStatement
        switch (item.getItemId())
        {
            case R.id.imei:
                break;

        }

        return super.onOptionsItemSelected(item);
    }
}
