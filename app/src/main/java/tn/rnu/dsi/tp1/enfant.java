package tn.rnu.dsi.tp1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

public class enfant extends AppCompatActivity {

    TextView poids,taille;
    Button ok,annuler;
    RadioGroup rg;
    RadioButton s,d;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enfant);
        poids=(TextView) findViewById(R.id.poids);
        taille=(TextView) findViewById(R.id.taille);
        ok=(Button)findViewById(R.id.ok);
        annuler=(Button)findViewById(R.id.annuler);
        rg=(RadioGroup)findViewById(R.id.rg);
        s=(RadioButton)findViewById(R.id.somme);
        d=(RadioButton)findViewById(R.id.difference);
        Bundle panier=this.getIntent().getExtras();
        if(panier!=null){
            //double p=panier.getDouble("poids");
            //double t=panier.getDouble("taille");
            poids.setText(""+panier.getDouble("poids"));
            taille.setText("" +panier.getDouble("taille"));
        }
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent resultat=new Intent();
                if(rg.getCheckedRadioButtonId()==R.id.somme){
                    resultat.putExtra("operateur","somme");

                }
                else {
                    resultat.putExtra("operateur","difference");

                }
                setResult(RESULT_OK,resultat);
                finish();

            }
        });
        annuler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });
    }


}